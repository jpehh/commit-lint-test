# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.2.0](https://gitlab.com/jpehh/commit-lint-test/-/compare/v1.1.0...v1.2.0) (2021-05-21)


### Features

* **escopo:** commit ([6946a5f](https://gitlab.com/jpehh/commit-lint-test/-/commit/6946a5f15c7e2c0730e8a0b1f35e593776e92be5))

### [1.1.1](https://gitlab.com/jpehh/commit-lint-test/-/compare/v1.1.0...v1.1.1) (2021-05-21)

## [1.1.0](https://gitlab.com/jpehh/commit-lint-test/-/compare/v1.0.0...v1.1.0) (2021-05-18)


### Features

* **standart version:** adds standart version as dependency ([d88d630](https://gitlab.com/jpehh/commit-lint-test/-/commit/d88d630e84436419f5c80e3bbd1c1b8a803dd59d))

## 1.0.0 (2021-05-18)
